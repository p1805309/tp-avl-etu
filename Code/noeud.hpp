#pragma once

struct Noeud {
  Noeud(int valeur) ;
  ~Noeud() ;

  Noeud* gauche ;
  Noeud* droite ;
  int valeur ;
  int hauteur;
} ;

int hauteur_noeud(const Noeud * n);
void update_hauteur(Noeud * n);