#include "noeud.hpp"
#include <algorithm>

Noeud::Noeud(int v) :
  gauche(nullptr),
  droite(nullptr),
  valeur(v)
{
  hauteur = 1;
}

Noeud::~Noeud() {
  delete gauche ;
  delete droite ;
}

int hauteur_noeud(const Noeud * n){
  int hauteur;
  
  if (n == nullptr)
  {
    return hauteur = 0;
  }
  else return n->hauteur;
}

void update_hauteur(Noeud * n){
  if (n != nullptr)
  {
  
  if(n->gauche == nullptr && n->droite == nullptr){
    n->hauteur = 1;
  }
    else if (n->gauche == nullptr )
  {
    n->hauteur = n->droite->hauteur + 1;
  }  

  else if (n->droite == nullptr )
  {
    n->hauteur = n->gauche->hauteur + 1;
  }
  else {
    n->hauteur = std::max(n->gauche->hauteur,n->droite->hauteur)+1;
  }
  }
}